# JIRA Sandbox

This project is intended to deploy any JIRA 7 versions to AWS.
There is Vagrant VirtualBox provider for local testing.
Docker support is planned too.

**Current status:** Early development

## How it Works

### Vagrant

If you have installed all prerequisites, then just type `vagrant status` to get an overview about available VMs.To run/kill a VM `vagrant up <name>` and `vagrant destroy [<name>]` commands are your friends.

### Git Secret

### Ansible

### Jenkins

## Prerequisites

### Local development

#### Mac OS X:

* git-secret extension
* VirtualBox 5.0 (VirtualBox 5.1 will fail because of vagrant-vbguest plugin)
* Vagrant 1.8.1 - 1.9.x

#### Linux (Ubuntu, CentOS)

* To be defined

#### Windows

* Not supported

### AWS staging

* To be defined

### AWS production

* To be defined
